#!/bin/bash

BASEDIR="/home/container"
export SERVER_JARFILE="${BASEDIR}/Waterfall.jar"

if [[ ! -e "${SERVER_JARFILE}" ]]; then
	echo "Downloading latest Waterfall build"
	if ! curl -SsL --output "${SERVER_JARFILE}" "https://papermc.io/ci/job/Waterfall/lastSuccessfulBuild/artifact/Waterfall-Proxy/bootstrap/target/Waterfall.jar"; then
		exit 1
	fi
fi

# Output Current Java Version
java -version

# Make internal Docker IP address available to processes.
export INTERNAL_IP=`ip route get 1 | awk '{print $NF;exit}'`
echo "IP today: $INTERNAL_IP"

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
eval ${MODIFIED_STARTUP}

# Example startup line: java -Xmx{{SERVER_XMX}}M -Xms{{SERVER_XMS}}M {{EXTRA_JVM}} -jar {{SERVER_JARFILE}} {{EXTRA_APP}}

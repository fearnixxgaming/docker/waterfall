# Dockerfile taken from Pterodactyl/Containers/java-glibc
FROM frolvlad/alpine-oraclejdk8:cleaned

MAINTAINER FearNixx Technik, <technik@fearnixx.de>

RUN apk update \
    && apk upgrade \
    && apk add --no-cache --update curl ca-certificates openssl git maven tar bash sqlite \
    && adduser -D -h /home/container container

USER container
ENV  USER=container HOME=/home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]
